from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Habit(models.Model):
    name = models.CharField(max_length=200)
    anzahl = models.FloatField(null=True, blank = True)
    umfang = models.CharField(max_length=200, null=True, blank = True)
    haufigkeit = models.IntegerField(default = 1)
    def __str__(self):
        return self.name


class Fortschritt(models.Model):
    habit = models.ForeignKey(Habit,on_delete=models.CASCADE)
    status = models.FloatField(null=True, blank = True)
    aktiv = models.BooleanField(default=False)
    erledigt = models.BooleanField(default=False)
    anfang = models.DateField()
    end = models.DateField()
    def __str__(self):
        return f"{self.anfang} {self.end} {self.habit.name}"


