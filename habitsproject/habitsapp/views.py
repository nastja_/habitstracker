from django.shortcuts import render, get_object_or_404, get_list_or_404
from . import models
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.utils import timezone
from datetime import datetime, timedelta, date
from django.db.models import Q

def index(request):
    habits = models.Habit.objects.all()
    heute = datetime.now().date()
    start = heute - timedelta(days=heute.weekday())
    end = start + timedelta(days=6)
    dates = [heute + timedelta(days=i) for i in range(0 - heute.weekday(), 7 - heute.weekday())]
    week = []
    for i in dates:
        week.append(str(i))
    print("Today: " + str(heute))
    print("Start: " + str(start))
    print("End: " + str(end))
    print(week)
    for item in models.Fortschritt.objects.all():
        if (item.anfang<=heute and item.end==heute) or (item.anfang==heute and item.end>=heute):
            item.aktiv= True
            item.save()
    fortschritte_week = models.Fortschritt.objects.filter(Q(anfang__range=[start, end]) | Q(end__range=[start, end])).order_by("anfang")
    week_dict = {}
    for item in habits:
        fortes_nach_days= {}
        for f in fortschritte_week:
            if f.habit == item:
                duration = f.end.day - f.anfang.day + 1
                if f.anfang < start:
                    duration -= (start.day - f.anfang.day + 1)
                if f.end > end:
                    duration -= (f.end.day- end.day +1)
                fortes_nach_days[f] = duration
        week_dict[item] = fortes_nach_days

    
    print(week_dict)
    context = {"habits": habits, "fortschritte": fortschritte_week, "heute": heute, "week_dict": week_dict, "week": week}
    return render(request, "habitsapp/index.html", context)

#def new_habit(request):
#    pass